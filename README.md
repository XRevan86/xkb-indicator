xkb-indicator 0.0.1
~~~~~~~~~~~~~~~~~~~
Copyright 2013-2015 Sorokin Alexei <sor.alexei@meowr.ru>

Description
-----------
xkb-indicator is an implementation of a status icon for XKB layout on
  Qt 4.7+.

Dependencies
-----------
gcc >= 4.4 | clang >= 3.0, xkbcommon, QtGui >= 4.7, QtCore >= 4.7,
qmake, xkb-utils

How to compile
--------------
qmake && make

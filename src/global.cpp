#include "global.h"

Options options;

// Public functions.

void Options::readAll() {
    // Storage for setVar argument: [0] is value, [1] is name, [2] is section;
    // If no section was added then writeAll() will not write this parameter to config.
    QStringList tempConfigData;

    QString xdgConfigHome = qgetenv("XDG_CONFIG_HOME");
    if (xdgConfigHome.isEmpty())
        xdgConfigHome = QDir::homePath() + "/.config";
    setVar("ConfigPath", QStringList(xdgConfigHome + "/" + projectName + ".ini"));

    if (!QDir().mkpath(xdgConfigHome))
        qWarning("Failed to create $XDG_CONFIG_HOME.");

    if (!QFile::exists(getStrVar("ConfigPath")) && QFile::exists("/etc/" + projectName + "/" + projectName + ".ini")) {
        if (QDir().exists(xdgConfigHome))
            QFile::copy("/etc/" + projectName + "/" + projectName + ".ini", getStrVar("ConfigPath"));
    }
    QSettings settings(getStrVar("ConfigPath"), QSettings::IniFormat);

    // text, flag or none.
    tempConfigData << iniRead(&settings, "Indicator", "IconType", "text") << "IconType" << "Indicator";
    setVar("IconType", tempConfigData); tempConfigData.clear();

    tempConfigData << iniRead(&settings, "Indicator", "ShowForSingle", "false").toLower() << "ShowForSingle"
      << "Indicator";
    setVar("ShowForSingle", tempConfigData); tempConfigData.clear();

    tempConfigData << iniRead(&settings, "Indicator", "TextColor", "auto") << "TextColor" << "Indicator";
    setVar("TextColor", tempConfigData); tempConfigData.clear();

    tempConfigData << iniRead(&settings, "Indicator", "CustomFlagDir", "") << "CustomFlagDir" << "Indicator";
    setVar("CustomFlagDir", tempConfigData); tempConfigData.clear();

    tempConfigData << iniRead(&settings, "Indicator", "SimpleMenu", "true").toLower() << "SimpleMenu"
      << "Indicator";
    setVar("SimpleMenu", tempConfigData); tempConfigData.clear();

    tempConfigData << iniRead(&settings, "Indicator", "EditLayout", "false").toLower() << "EditLayout"
      << "Indicator";
    setVar("EditLayout", tempConfigData); tempConfigData.clear();

    if (getBoolVar("EditLayout")) {
        tempConfigData << iniRead(&settings, "Layout", "Model", "pc105") << "Model" << "Layout";
        setVar("Model", tempConfigData); tempConfigData.clear();

        tempConfigData << iniRead(&settings, "Layout", "Layouts", "us") << "Layouts" << "Layout";
        setVar("Layouts", tempConfigData); tempConfigData.clear();

        tempConfigData << iniRead(&settings, "Layout", "Variants", " ,") << "Variants" << "Layout";
        setVar("Variants", tempConfigData); tempConfigData.clear();

        tempConfigData << iniRead(&settings, "Layout", "Options", "grp:alt_shift_toggle,grp_led:scroll")
          << "Options" << "Layout";
        setVar("Options", tempConfigData); tempConfigData.clear();
    }
    // Write settings instantly.
    writeAll();
}

void Options::writeAll() {
    QSettings settings(getStrVar("ConfigPath"), QSettings::IniFormat);

    foreach(QString name, variables.keys())
        if (getVar(name).count() > 2 && !getVar(name).at(1).isNull())
            iniWrite(&settings, getVar(name).at(2), getVar(name).at(1), getVar(name).at(0));
}

QStringList Options::getVar(const QString &name) {
    return variables.value(name);
}

QString Options::getStrVar(const QString &name) {
    return getVar(name).isEmpty() ? QString() : getVar(name).at(0);
}

bool Options::getBoolVar(const QString &name) {
    if (getStrVar(name).toLower() == "false")
        return false;
     else
        return true;
}

void Options::setVar(const QString &name, const QStringList &value) {
    variables.insert(name, value);
}

QString Options::iniRead(QSettings *settings, const QString &section, const QString &value,
  const QString &defaultValue) {
    QString result;
    settings->beginGroup(section);
    result = settings->value(value, "").toString();
    if (result.isNull() || result.isEmpty())
        result = defaultValue;
    settings->endGroup();
    return result;
}

bool Options::iniWrite(QSettings *settings, const QString &section, const QString &value,
  const QString &inValue) {
    if (settings->isWritable()) {
        settings->beginGroup(section);
        settings->setValue(value, inValue);
        settings->endGroup();
    } else
        return false;
    return true;
}

void window_about(QWidget *parent = 0) {
    QMessageBox::about(parent, "About " + projectName,
      "<h2>" + projectName + " v" + projectVer + "</h2>" +
      "<b>An implementation of a status icon for XKB layout on Qt.</b>" +
      "<p>" + copyrightYear + " (c) " + copyright);
}

/* Copyright (C) 2015 Sorokin Alexei <sor.alexei@meowr.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License */

#ifndef GLOBAL_H
#define GLOBAL_H

#include <QtGui>
#ifndef IS_QT4
#include <QtWidgets>
#endif

const QString projectName = "xkb-indicator";
const QString projectVer = "0.0.1";
const QString copyright = "Sorokin Alexei";
const QString copyrightYear = "2015";

class Options: public QObject {
    Q_OBJECT

public:
    void readAll();
    void writeAll();

    QStringList getVar(const QString &name);
    QString getStrVar(const QString &name);
    bool getBoolVar(const QString &name);
    void setVar(const QString &name, const QStringList &value);

    QString iniRead(QSettings *settings, const QString &section, const QString &value,
      const QString &defaultValue);
    bool iniWrite(QSettings *settings, const QString &section, const QString &value,
      const QString &inValue);

private:
    QHash<QString, QStringList> variables;
};

void window_about(QWidget *parent);

extern Options options;

#endif // global.h

#include "indicator.h"

// Public functions.

Indicator::Indicator() {
    statusIcon = NULL;
    mainMenu = NULL;
    settingsWindow = NULL;
    // updater() checks the match with curLayout: it could not update icons properly if curLayout would be
    //  zero of else by default.

    applyPreferences();
    updaterTimer.start(timeout);
    QObject::connect(qApp, SIGNAL(messageReceived(QString)), this, SLOT(receiveFromInstance(QString)));
    if (qApp->arguments().contains("--settings"))
        showSettings();
}

Indicator::~Indicator() {
    if (statusIcon)
        delete(statusIcon);
    if (mainMenu) {
        delete(layoutsMenu);
        delete(mainMenu);
    }
    if (settingsWindow)
        delete(settingsWindow);
}

void Indicator::applyPreferences() {
    options.readAll();
    if (options.getBoolVar("EditLayout")) {
        xkl.setXkbMap(options.getStrVar("Model"), options.getStrVar("Layouts").split(','),
          options.getStrVar("Variants").split(','), options.getStrVar("Options").split(','));
    }
    if ((options.getStrVar("IconType") == "none") ||
      (options.getBoolVar("ShowForSingle") && xkl.groupSymbols().count() == 1)) {
        disconnect(&updaterTimer, SIGNAL(timeout()), this, SLOT(updater()));
        if (statusIcon)
            delete(statusIcon);
        if (mainMenu) {
            delete(layoutsMenu);
            delete(mainMenu);
        }
    } else {
        groupIcons.clear();
        createMenu();
        if (!createIcon()) {
            qWarning() << "Unable to place status icon.";
            qApp->quit();
        }
        connect(&updaterTimer, SIGNAL(timeout()), this, SLOT(updater()));
    }
}

// Private functions.

bool Indicator::createIcon() {
    if (!statusIcon)
        statusIcon = new QSystemTrayIcon(this);

    // We have to know the size of the icon, but without show() it's unavailable
    //  and without setIcon() show() is.
    QPixmap emptyPixmap(1, 1);
    emptyPixmap.fill(Qt::transparent);
    if (statusIcon->isSystemTrayAvailable()) {
        statusIcon->setContextMenu(mainMenu);
        connect(statusIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
          this, SLOT(iconActionActivated(QSystemTrayIcon::ActivationReason)));

        statusIcon->setIcon(QIcon(emptyPixmap));
        statusIcon->show();
    } else
        return false;
    return true;
}

void Indicator::createMenu() {
    if (mainMenu)
        mainMenu->clear();
    else
        mainMenu = new QMenu;
    layoutsMenu = mainMenu->addMenu("Layouts");
    mainMenu->addAction(QIcon::fromTheme("preferences-desktop-keyboard"),
      "Keyboard Preferences")->setData("keyPrefs");
    if (!options.getBoolVar("SimpleMenu")) {
        mainMenu->addSeparator();
        mainMenu->addAction(QIcon::fromTheme("help-about"), "About")->setData("about");
        mainMenu->addAction("About Qt")->setData("aboutQt");
        mainMenu->addSeparator();
        mainMenu->addAction(QIcon("application-exit"), "Exit")->setData("exit");
    }
    connect(mainMenu, SIGNAL(triggered(QAction*)), SLOT(menuActionActivated(QAction*)));
}

QIcon Indicator::generateTextIcon(const QString &name) {
    int statusIconSize = statusIcon->geometry().height();
    QPainter painter;
    QPixmap pixmap(statusIconSize, statusIconSize);
    QColor color;
    if (options.getStrVar("TextColor").toLower() == "auto")
        color = QWidget().palette().toolTipText().color();
    else
        color.setNamedColor(options.getStrVar("TextColor").toUpper());

    pixmap.fill(Qt::transparent);
    painter.begin(&pixmap);
    if (painter.isActive()) {
        painter.setPen(color);
        painter.setFont(QFont("sans-serif", int(float(statusIconSize) * 0.55)));
        painter.drawText(pixmap.rect(), Qt::AlignCenter, name);
        painter.end();
    }
    return QIcon(pixmap);
}

QIcon Indicator::generateFlagIcon(const QString &name) {
    QIcon result;
    QStringList pathList;
    // Higher in list — higher in priority.
    if (!options.getStrVar("CustomFlagDir").isNull())
        pathList.append(options.getStrVar("CustomFlagDir"));
    pathList.append(QDir::homePath() + "/.icons/flags/");
    pathList.append("/usr/share/flags/countries/16x11/");
    foreach(QString fileName, pathList)
        foreach(QString fileResolution, QImageWriter::supportedImageFormats())
            if (QFile::exists(fileName + name + "." + fileResolution)) {
                result = QIcon(fileName + name + "." + fileResolution);
                break;
            }
    // If no flag icon was found.
    if (result.isNull())
        result = generateTextIcon(name);
    return result;
}

void Indicator::generateIcons(const int layoutsCount) {
    QString layoutName;
    for (int counter = 0; counter < layoutsCount; counter++) {
        layoutName = savedGroupSymbols.at(counter);
        if (!groupIcons.contains(layoutName)) {
            if (options.getStrVar("IconType") == "flag")
                groupIcons.insert(layoutName, generateFlagIcon(layoutName));
            else
                groupIcons.insert(layoutName, generateTextIcon(layoutName));
        }
    }
}

void Indicator::switchLayout(const bool forward) {
    int currentLayout = xkl.currentGroupNum(), groupCount = xkl.groupSymbols().count();
    if (forward) {
        currentLayout++;
        if (currentLayout >= groupCount)
            currentLayout = 0;
    } else {
        currentLayout--;
        if (currentLayout == -1)
            currentLayout = groupCount - 1;
    }
    xkl.setGroupByNum(currentLayout);
}

void Indicator::updateMenu(const QStringList &groupNamesList, const int groupCount) {
    QStringList layout;
    layoutsMenu->clear();
    for(int counter = 0; counter < groupCount; counter++) {
        layout.clear();
        layout.append("layouts"); layout.append(QString::number(counter));
        layoutsMenu->addAction(groupNamesList.at(counter))->setData(layout);
    }
}

void Indicator::showSettings(QWidget *parent) {
    if (!settingsWindow)
        settingsWindow = new SettingsWindow(parent);
    settingsWindow->show();
}

// private slots.

void Indicator::updater() {
    QStringList groupSymbols = xkl.groupSymbols();

    // It is useless to update icons and menu if a list isn't changed.
    if (savedGroupSymbols != groupSymbols) {
        savedGroupSymbols = groupSymbols;
        generateIcons(groupSymbols.count());
        updateMenu(xkl.groupNames(), groupSymbols.count());
    }
    if (groupSymbols.count() < savedGroupNum)
            xkl.setGroupByNum(0);
    if (savedGroupNum != xkl.currentGroupNum()) {
        savedGroupNum = xkl.currentGroupNum();
        QIcon currentIcon = groupIcons.value(groupSymbols.at(savedGroupNum));
        if (!currentIcon.isNull())
            statusIcon->setIcon(currentIcon);
        statusIcon->setToolTip(xkl.currentGroupName());
    }
}

void Indicator::iconActionActivated(const QSystemTrayIcon::ActivationReason &reason) {
    switch(reason) {
      case QSystemTrayIcon::Trigger:
          switchLayout(true);
          break;
      case QSystemTrayIcon::MiddleClick:
        switchLayout(false);
        break;
      default:
         ;
    }
}

void Indicator::menuActionActivated(QAction *action) {
    QStringList ident = action->data().toStringList();
    if (ident.at(0) == "layouts") {
        xkl.setGroupByNum(ident.at(1).toInt());
    } else if (ident.at(0) == "keyPrefs")
        showSettings(this);
    else if (ident.at(0) == "about")
        window_about(this);
    else if (ident.at(0) == "aboutQt")
        qApp->aboutQt();
    else if (ident.at(0) == "exit")
        qApp->quit();
}

void Indicator::receiveFromInstance(const QString &message) {
    if (message == "showSettings")
        showSettings();
}

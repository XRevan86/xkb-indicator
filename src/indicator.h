/* Copyright (C) 2015 Sorokin Alexei <sor.alexei@meowr.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version. */

#ifndef INDICATOR_H
#define TINDICATOR_H

#include <QtGui>
#ifndef IS_QT4
#include <QtWidgets>
#endif
#include "xkl-wrapper.h"

#include "global.h"
#include "settingswindow.h"

class Indicator: public QWidget {
    Q_OBJECT

public:
    Indicator();
    ~Indicator();
    void applyPreferences();

private:
    const static int timeout = 150;

    int savedGroupNum;
    QStringList savedGroupSymbols;
    QHash<QString, QIcon> groupIcons;
    QSystemTrayIcon *statusIcon;
    XKL_wrapper xkl;
    QTimer updaterTimer;
    SettingsWindow *settingsWindow;
    QMenu *mainMenu;
    QMenu *layoutsMenu;

    void createMenu();
    bool createIcon();
    QIcon generateTextIcon(const QString &name);
    QIcon generateFlagIcon(const QString &name);
    void generateIcons(const int layoutsCount);
    void switchLayout(const bool forward = true);
    void updateMenu(const QStringList &groupNamesList, const int groupCount);
    void showSettings(QWidget *parent = 0);

private slots:
    void updater();
    void iconActionActivated(const QSystemTrayIcon::ActivationReason &reason);
    void menuActionActivated(QAction *action);
    void receiveFromInstance(const QString &message);

};

#endif // indicator.h

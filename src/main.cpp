/* Copyright (C) 2015 Sorokin Alexei <sor.alexei@meowr.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License */

#include "main.h"

int main(int argc, char *argv[]) {
    DSingleApplication app(projectName, argc, argv);

    app.setApplicationName(projectName);
    app.setApplicationVersion(projectVer);
    app.setQuitOnLastWindowClosed(false);
#ifdef IS_QT4
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    if (qgetenv("DESKTOP_SESSION") == "mate")
        QApplication::setStyle(QStyleFactory::create("GTK+"));
#endif

    if (app.isRunning()) {
        if (app.arguments().contains("--settings"))
            app.sendMessage("showSettings");
        else
            qWarning("Error: application is already running.");
        return 0;
    }

    Indicator indicator;
    // Oh Great Maker, I hate warnings!
    (void) indicator;

    return app.exec();
}

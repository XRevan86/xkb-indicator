#include "settingswindow.h"

SettingsWindow::SettingsWindow(QWidget *parent):
    QDialog(parent), ui(new Ui::SettingsWindow) {
    ui->setupUi(this);
}

SettingsWindow::~SettingsWindow() {
    delete(ui);
}

void SettingsWindow::on_oldLayoutCheckbox_clicked(const bool checked) {
    true;
}

void SettingsWindow::on_helpButton_clicked() {
    window_about(this);
}

void SettingsWindow::on_closeButton_clicked() {
    this->close();
}

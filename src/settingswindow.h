/* Copyright (C) 2015 Sorokin Alexei <sor.alexei@meowr.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version. */

#ifndef SETTING_SWINDOW_H
#define SETTING_SWINDOW_H

#include <QtGui>
#ifndef IS_QT4
#include <QtWidgets>
#endif

#include "global.h"
#include "ui_settingswindow.h"

namespace Ui {
    class SettingsWindow;
}

class SettingsWindow: public QDialog {
    Q_OBJECT
    
public:
    explicit SettingsWindow(QWidget *parent = 0);
    ~SettingsWindow();

private slots:
    void on_oldLayoutCheckbox_clicked(const bool checked);
    void on_helpButton_clicked();
    void on_closeButton_clicked();

private:
    Ui::SettingsWindow *ui;
};

#endif // settingswindow.h

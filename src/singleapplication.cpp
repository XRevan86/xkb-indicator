#include "singleapplication.h"

DSingleApplication::DSingleApplication(const QString &id, int &argc, char *argv[]):
  QApplication(argc, argv) {
    other_instance_running = false;
    app_id = id;
    port = d_unique_port_start;

    tcpServer = new DTalker(app_id, this);
    tcpSocket = NULL;

    // Start at d_unique_port_start and go until find a free port or a port
    // that answers correctly.
    other_instance_running = false;

    DPortChecker checker(app_id, d_unique_port_start, this);

    // First go over the range of ports and check for other instance
    // if not, then listen on the forst port available.
    DPortList ports;

    while (port <= d_unique_port_finish) {
        // Here check if the stuff running on port is our instance if not procede.
        checker.check( port );
        checker.wait();
        DPortChecker::PortStatus port_status = checker.status();

        if (port_status == DPortChecker::us) {
            other_instance_running = true;
            // Here we have to connect to other instance to send messages.
            tcpSocket = checker.transferSocketOwnership();
            return;
        }

        DPortInfo pi(port, checker.status() == DPortChecker::free);
        ports << pi;
        ++port;
    }

    port = ports.firstFreePort();

    // other instance is not running in the range and there's available port
    if (port == -1)
        return;

    // this port is free and current instance is in listening mode
    bool listening = tcpServer->listen( QHostAddress::LocalHost, port );
    if (listening)
        connect(tcpServer, SIGNAL(messageReceived(const QString&)), this, SLOT(onClientMessage(const QString&)));
}

DSingleApplication::~DSingleApplication() {
    if (tcpServer)
        delete(tcpServer);
    if (tcpSocket)
        delete(tcpSocket);
}

QString DSingleApplication::id() {
    return app_id;
}

bool DSingleApplication::isRunning () {
    // May require some checks here.
    return other_instance_running;
}

bool DSingleApplication::sendMessage(const QString &message) {
    if (!other_instance_running)
        return false;
    if (!tcpSocket)
        return false;
    if (tcpSocket->state() != QAbstractSocket::ConnectedState)
        return false;

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(out.version()); // Set to the current Qt version.
  
    QString msg = app_id + ":" + message;
    out << (quint32) msg.size();
    out << msg;

    tcpSocket->write(block);
    tcpSocket->flush();
    tcpSocket->waitForBytesWritten(d_timeout_try_write);

    return true;
}

void DSingleApplication::onClientMessage(const QString &message) {
    emit messageReceived(message);
}

// This class is used to check specific port if it has an instance of this app.

DPortChecker::DPortChecker(const QString &id, int port, QObject *parent): QThread(parent) {
    app_id = QString(id);
    tcpSocket = NULL;
    this->port = port;
    result = DPortChecker::free;
}

DPortChecker::~DPortChecker() {
    if (tcpSocket)
        delete(tcpSocket);
}

DPortChecker::PortStatus DPortChecker::status() {
    return result;
}

void DPortChecker::check(int port) {
    this->port = port;
    start();
}

void DPortChecker::run() {
    result = DPortChecker::free;

    if (tcpSocket == NULL)
        tcpSocket = new QTcpSocket();

    tcpSocket->connectToHost(QHostAddress(QHostAddress::LocalHost), port);
    if (!tcpSocket->waitForConnected(d_timeout_try_connect)) {
        tcpSocket->abort();
        return;
    }

    result = DPortChecker::others;
    if (!tcpSocket->waitForReadyRead(d_timeout_try_read) ) {
        tcpSocket->abort();
        return;
    }

    // Now compare received bytes with app_id.
    QDataStream in(tcpSocket);
    in.setVersion(in.version()); // Set to the current Qt version.

    if (tcpSocket->bytesAvailable() > 0) {
        QString msgString;
        in >> msgString;
        if (msgString.size() <= 1) {
            tcpSocket->abort();
            return;
        }
        int s = qMin(msgString.size(), app_id.size());
        if (QString::compare(msgString.left(s), app_id.left(s)) == 0)
            result = DPortChecker::us;
    }
}

QTcpSocket *DPortChecker::transferSocketOwnership() {
    QTcpSocket *tmp = tcpSocket;
    tcpSocket = NULL;
    return tmp;
}

// This is a server responsible to talking to incoming connections.

DTalker::DTalker(const QString &id, QObject *parent): QTcpServer(parent) {
    app_id = QString(id);
}

void DTalker::incomingConnection( int socketDescriptor ) {
    DListner *listner = new DListner(app_id, socketDescriptor, this);
    connect(listner, SIGNAL(messageReceived(const QString&)), this, SLOT(onClientMessage(const QString&)));
    connect(listner, SIGNAL(finished()), listner, SLOT(deleteLater()));
    listner->start();
}

void DTalker::onClientMessage(const QString &message) {
    emit messageReceived( message );
}

// This thread is used to communicate with connected client.

DListner::DListner(const QString &id, int _socketDescriptor, QObject *parent): QThread(parent) {
    app_id = QString(id);
    socketDescriptor = _socketDescriptor;
    blockSize = 0;
}

void DListner::run() {
    QTcpSocket tcpSocket;
    if (!tcpSocket.setSocketDescriptor(socketDescriptor))
        return;

    // Send app_id to the client.
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(out.version()); // Set to the current Qt version instead.
    out << app_id;
    tcpSocket.write(block);
    //waitForBytesWritten(int msecs)

    while (true) {
        if (tcpSocket.state() != QAbstractSocket::ConnectedState)
            return;
        tcpSocket.waitForReadyRead(-1);
        read(&tcpSocket);
    }
}

void DListner::read( QTcpSocket *tcpSocket ) {
    if (tcpSocket == NULL)
        return;
    if (tcpSocket->state() != QAbstractSocket::ConnectedState)
        return;

    QDataStream in(tcpSocket);
    in.setVersion(in.version()); // Set to the current Qt version instead.

    if (blockSize == 0) {
        if (tcpSocket->bytesAvailable() < (int) sizeof(quint32))
            return;
        in >> blockSize;
    }
    if (tcpSocket->bytesAvailable() < blockSize)
        return;
    QString msgString;
    in >> msgString;
  
    // If header matches, trim and emit.
    QString magic = app_id + ":";
    if (QString::compare(msgString.left(magic.size()), magic) == 0)
        emit messageReceived(msgString.remove(0, magic.size()));

    blockSize = 0;
    if (tcpSocket->bytesAvailable() > 0)
        read(tcpSocket);
}

int DPortList::firstFreePort() {
    DPortList::iterator it = this->begin();
    while (it < this->end()) {
        if (it->free)
            return it->port;
        it++;
    }
    return -1;
}

bool DPortList::freePortAvailable() {
    int p = firstFreePort();
    return p != -1;
}

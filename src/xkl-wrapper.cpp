#include "xkl-wrapper.h"

QStringList tempList;

// XKL_wrapper ----------------------------------------------------

XKL_wrapper::XKL_wrapper(): display(0) {
    display = XOpenDisplay(NULL);

    engine = xkl_engine_get_instance(display);
    config = xkl_config_registry_get_instance(engine);
    xkl_config_registry_load(config, false);
}

XKL_wrapper::~XKL_wrapper() {
    XCloseDisplay(display);
}

QStringList XKL_wrapper::groupSymbols(bool names) {
    QStringList result;
    unsigned int counter = 0;
    xkl_engine_start_listen(engine, XKLL_TRACK_KEYBOARD_STATE);
    XklConfigRec *record = xkl_config_rec_new();
    if (!xkl_config_rec_get_from_server(record, engine))
        return QStringList();
    if (!names)
        while( record->layouts[counter] ) {
            result.append(record->layouts[counter]);
            counter++;
        }
    else
        while( xkl_engine_get_groups_names(engine)[counter] ) {
            result.append(xkl_engine_get_groups_names(engine)[counter]);
            counter++;
        }
    xkl_config_rec_reset(record);
    xkl_engine_stop_listen(engine, XKLL_TRACK_KEYBOARD_STATE);
    return result;
}

QStringList XKL_wrapper::groupNames() {
    return groupSymbols(true); // names = true
}

QStringList XKL_wrapper::groupVariants() {
    tempList.clear();
    xkl_config_registry_foreach_layout_variant(config, currentGroupSymbol().toLocal8Bit().data(),
      (XklConfigItemProcessFunc) group_symbol_list_create, NULL);
    QStringList result = tempList;
    tempList.clear();
    xkl_engine_stop_listen(engine, XKLL_MANAGE_LAYOUTS);
    return result;
}

QStringList XKL_wrapper::allGroupSymbols() {
    QStringList result;
    tempList.clear();
    xkl_config_registry_foreach_layout(config, (XklConfigItemProcessFunc) group_symbol_list_create, NULL);
    result = tempList;
    tempList.clear();
    return result;
}

QStringList XKL_wrapper::allGroupNames() {
    tempList.clear();
    xkl_config_registry_foreach_layout(config, (XklConfigItemProcessFunc) group_symbol_list_create, NULL);
    QStringList result = tempList;
    tempList.clear();
    return result;
}

int XKL_wrapper::currentGroupNum() {
    xkl_engine_start_listen(engine, XKLL_TRACK_KEYBOARD_STATE);
    int result = xkl_engine_get_current_state(engine)->group;
    xkl_engine_stop_listen(engine, XKLL_TRACK_KEYBOARD_STATE);
    return result;
}

QString XKL_wrapper::currentGroupSymbol() {
    return groupSymbols().at(currentGroupNum());
}

QString XKL_wrapper::currentGroupName() {
    return QString().append(groupNames().at(currentGroupNum()));
}

QString XKL_wrapper::currentGroupVariant() {
    return groupVariants().at(currentGroupNum());
}

bool XKL_wrapper::setGroupByNum(const unsigned int groupNum) {
    unsigned int groupCount = groupSymbols().count();
    if (groupNum > groupCount || groupCount <= 1)
        return false;
    xkl_engine_lock_group(engine, groupNum);
    return true;
}

bool XKL_wrapper::setXkbMap(QString model, QStringList layout, QStringList variant, QStringList option) {
    QStringList args;
    args.append("-model"); args.append(model);
    args.append("-layout"); args.append(layout.join(","));
    args.append("-variant"); args.append(variant.join(","));
    args.append("-option"); args.append(option.join(","));
    if (QProcess::execute("setxkbmap", args) != 0) {
        qDebug("Failed to activate new configuration");
        return false;
    }
    return true;
}

// Helper functions -----------------------------------------------

void group_symbol_list_create(XklConfigRegistry *config_registry, XklConfigItem *config_item, bool names) {
    if (!names)
        tempList.append(QString().append(config_item->name));
    else
        tempList.append(QString().append(config_item->description));
    // Stupid warnings…
    config_registry = config_registry;
}

void groupNameListCreate(XklConfigRegistry *config_registry, XklConfigItem *config_item) {
    group_symbol_list_create(config_registry, config_item, true);
}

const char **stringlist_to_stringarray(QStringList stringList) {
    int counter = 0;
    char **resultVar = new char*[stringList.size() + 1];
    for(counter = 0; counter < stringList.size(); counter++) {
        resultVar[counter] = new char[strlen(stringList.at(counter).toLatin1()) + 1];
        memcpy(resultVar[counter], stringList.at(counter).toLatin1(),
          strlen(stringList.at(counter).toLatin1()) + 1);
    }
    resultVar[stringList.size()] = ((char) NULL);
    const char **result = (const char**) resultVar;
    counter = 0;
    while (resultVar[counter]) {
        delete(resultVar[counter]);
        counter++;
    }
    delete(resultVar);
    return result;
}

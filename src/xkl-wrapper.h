/* Copyright (C) 2015 Sorokin Alexei <sor.alexei@meowr.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version. */

#ifndef XKL_WRAPPER_H
#define XKL_WRAPPER_H

#include <QtCore>
#include <X11/X.h>
#include <libxklavier/xklavier.h>
#include <exception>

extern QStringList tempList;

// C++ exception that wraps X11 errors.
class X11Exception : public std::exception {

public:
    X11Exception(): _reason("unknown") {}
    X11Exception(const QString &what): _reason(what) {}
    virtual ~X11Exception() throw () {}
    virtual const char *what() const throw () {
        return _reason.toLatin1().data();
    }

private:
    QString _reason;
};

// XKL_wrapper ----------------------------------------------------

class XKL_wrapper: public QObject {
    Q_OBJECT

public:
    XKL_wrapper();
    ~XKL_wrapper();
    QStringList groupSymbols(bool names = false);
    QStringList groupNames();
    QStringList groupVariants();
    QStringList allGroupSymbols();
    QStringList allGroupNames();
    int currentGroupNum();
    QString currentGroupSymbol();
    QString currentGroupName();
    QString currentGroupVariant();
    bool setGroupByNum(const unsigned int groupNum);
    bool setXkbMap(QString model, QStringList layout, QStringList variant, QStringList option);

private:
    Display *display;
    XklEngine *engine;
    XklConfigRegistry *config;
};

// Helper functions -----------------------------------------------

void group_symbol_list_create(XklConfigRegistry *config_registry = NULL, XklConfigItem *config_item = NULL,
  bool names = false);
void group_name_list_create(XklConfigRegistry *config_registry = NULL, XklConfigItem *config_item = NULL);
const char **stringlist_to_stringarray(QStringList stringList);

#endif // xkl-wrapper.h

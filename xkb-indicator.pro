QT += core gui network
lessThan(QT_MAJOR_VERSION, 5) {
        DEFINES += IS_QT4
} else {
        QT += widgets
}

TARGET = xkb-indicator
TEMPLATE = app
CONFIG += thread link_pkgconfig
PKGCONFIG += glib-2.0 x11 libxklavier

SOURCES += src/main.cpp \
    src/global.cpp \
    src/indicator.cpp \
    src/settingswindow.cpp \
    src/singleapplication.cpp \
    src/xkl-wrapper.cpp

HEADERS += src/main.h \
    src/global.h \
    src/indicator.h \
    src/settingswindow.h \
    src/singleapplication.h \
    src/xkl-wrapper.h

FORMS += ui/settingswindow.ui
